require('../css/styles.css');

const pluginName = 'auto-login';
const pluginVersion = '1.0.0';

const containerName = pluginName + '-container';

let $ = window.$;
if ($ === undefined && minerva.$ !== undefined) {
  $ = minerva.$;
}

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;
let minervaVersion;

const register = async function (_minerva) {

  $(".tab-content").css('position', 'relative');

  minervaProxy = _minerva;
  pluginContainer = $(minervaProxy.element);
  pluginContainerId = pluginContainer.attr('id');
  if (!pluginContainerId) {
    //the structure of plugin was changed at some point and additional div was added which is the container but does not have any properties (id or height)
    pluginContainerId = pluginContainer.parent().attr('id');
  }

  let conf = await minerva.ServerConnector.getConfiguration();
  minervaVersion = parseFloat(conf.getVersion().split('.').slice(0, 2).join('.'));
  console.log('minerva version: ', minervaVersion);
  await doLogin(false);
  return initPlugin();
};

const unregister = function () {
  console.log('unregistering ' + pluginName + ' plugin');
};

const getName = function () {
  return pluginName;
};

const getVersion = function () {
  return pluginVersion;
};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function () {
  return {
    register: register,
    unregister: unregister,
    getName: getName,
    getVersion: getVersion,
    minWidth: 400,
    defaultWidth: 500
  }
});

async function getParam(paramName) {
  try {
    let result = await minervaProxy.pluginData.getGlobalParam(paramName);
    try {
      let object = JSON.parse(result);
      return object.value;
    } catch (e) {
      return result;
    }
  } catch (error) {
    return "";
  }
}

async function initPlugin() {
  initMainPageStructure();
  let login = await getParam("plugin-login");
  let password = await getParam("plugin-password");
  $(".input-plugin-login", pluginContainer).val(login);
  $(".input-plugin-password", pluginContainer).val(password);
  $(".auto-login-button", pluginContainer).on('click', async function () {
    await update();
    await doLogin(true);
  });

}

async function update() {
  await minervaProxy.pluginData.setGlobalParam("plugin-login", $(".input-plugin-login", pluginContainer).val())
  await minervaProxy.pluginData.setGlobalParam("plugin-password", $(".input-plugin-password", pluginContainer).val())
}

async function doLogin(showWarning = false) {
  let login = await getParam("plugin-login");
  let password = await getParam("plugin-password");

  let currentUserLogin = minerva.ServerConnector.getSessionData().getLogin();
  if (currentUserLogin === null || currentUserLogin === undefined || currentUserLogin === 'anonymous') {
    if (login !== '') {
      try {
        await minerva.ServerConnector.login(login, password);
        window.location.href = removeParam("plugins", window.location.href);
        return true;
      } catch (error) {
        if (showWarning) {
          alert("Invalid credentials");
        }
      }
    }
  } else if (login === currentUserLogin) {
    window.location.href = removeParam("plugins", window.location.href);
  }
  return false;
}

function removeParam(key, sourceURL) {
  var rtn = sourceURL.split("?")[0],
    param,
    params_arr = [],
    queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
  if (queryString !== "") {
    params_arr = queryString.split("&");
    for (var i = params_arr.length - 1; i >= 0; i -= 1) {
      param = params_arr[i].split("=")[0];
      if (param === key) {
        params_arr.splice(i, 1);
      }
    }
    rtn = rtn + "?" + params_arr.join("&");
  }
  return rtn;
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function initMainPageStructure() {

  const container = $(`<div class=${containerName}></div>`).appendTo(pluginContainer);
  container.append('<h4>Credentials</h4>');
  container.append(`
        <form class="form-horizontal">
            <div class="form-group row">
                <label class="col-sm-2 control-label">Login</label>
                <div class="col-sm-10">
                    <input class="input-plugin-login form-control" value="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input class="input-plugin-password form-control" value="">
                </div>
            </div>                        
        </form>
        <button type="button" class="btn-minerva btn btn-primary btn-default btn-block auto-login-button">Save and login</button>
    `);
}
